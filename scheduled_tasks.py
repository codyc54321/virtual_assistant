#!/usr/bin/env python

import datetime, schedule, time

from messager.send_texts import send_text
from messager.send_emails import send_email
from data.tasks import TASKS, TEXT_PREMESSAGE

DAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']

LUNCH_TIMES = ['12:00'] # 12pm
NIGHT_TIMES = ['17:00'] # 5pm


def get_tasks_text_message():
    return TEXT_PREMESSAGE + "\n\n" + TASKS

def schedule_reminders(days, times, schedule_package):
    for day in days:
        for time in times:
            getattr(schedule.every(), day).at(time).do(send_email, subject="Do your tasks!", body=TASKS).tag('tasks')
            getattr(schedule.every(), day).at(time).do(
                send_text,
                body=get_tasks_text_message(),
                target_phone_number='+15127365653'
            ).tag('tasks')


if __name__ == '__main__':
    schedule.every(2).minutes.do(
        send_email,
        subject="ya"
    )
    # # M-Th at lunch
    # schedule_reminders(DAYS[0:5], LUNCH_TIMES, schedule)
    #
    # # M/W when I'm home from gym
    # schedule_reminders(DAYS[1:2], NIGHT_TIMES, schedule) # tuesday 2am is monday 9pm
    # schedule_reminders(DAYS[3:4], NIGHT_TIMES, schedule) # thursday 2am is wednesday 9pm
    #
    # # Sa when I'm home from gym
    # schedule_reminders(DAYS[5:6], ['21:00'], schedule)
    #
    # # Su when I'm home reading
    # schedule_reminders(DAYS[6:7], ['12:00'], schedule)
    # schedule_reminders(DAYS[6:7], ['18:00'], schedule)

    while True:
        """ Prove how to clear the schedule...
        https://schedule.readthedocs.io/en/stable/faq.html#clear-job-by-tag
        """
        # you can clear tasks if you give them a tag using .tag('tag-name')...
        # schedule.clear('tasks')
        # schedule.clear('id_23')

        schedule.run_pending()
        time.sleep(60)
