#!/bin/bash

# get background job id and kill it
ps -ef | grep python | awk '{print $2}' | xargs kill -9

cd /src/virtual_assistant

git checkout master
git pull origin master

python /src/virtual_assistant/scheduled_tasks.py &
