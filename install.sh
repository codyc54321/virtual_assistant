#!/bin/bash



sudo apt update -y
sudo apt install build-essential python-dev -y
sudo apt install python-pip -y

# ensures that common C headers are installed
apt-get install build-essential autoconf libtool pkg-config python-opengl python-imaging python-pyrex python-pyside.qtopengl idle-python2.7 qt4-dev-tools qt4-designer libqtgui4 libqtcore4 libqt4-xml libqt4-test libqt4-script libqt4-network libqt4-dbus python-qt4 python-qt4-gl libgle3 python-dev libssl-dev -y

mkdir /src

if [ -e /srcvirtual_assistant/client_secret.json ]
then
    echo "client secret found"
else
    echo "Please run the following command:"
    echo "   scp /path/to/virtual_assistant/client_secret.json root@{DROPLET_IP}:/src/virtual_assistant"
    exit 1


git clone https://bitbucket.org/codyc54321/virtual_assistant /src/virtual_assistant


# install the cronjob to restart it any time the code changes:
# http://stackoverflow.com/questions/878600/how-to-create-a-cron-job-using-bash
crontab -l > mycron
echo "* * * * * /bin/bash /src/virtual_assistant/restart.sh" >> mycron
crontab mycron
rm mycron

python /src/virtual_assistant/scheduled_tasks.py &
