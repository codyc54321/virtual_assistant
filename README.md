# Virtual assistant

A working repo in use today, that texts and emails me a list of tasks to do during my downtime (lunch, Sunday afternoon, etc)

I was surprised to not be able to find an app that sends you batch reminders of everything you need to do at certain times of day,
different days of the week, so I wrote one myself

## Configuring Gmail

Get your client_secret.json from this document: https://developers.google.com/gmail/api/auth/web-server
Notice client_secret.json is ignored in .gitignore...this is needed to send emails to yourself from a server and substitutes for logging in with your gmail's password.

## Config

Change config/__init__example.py to config/__init__.py

Make a Twilio account and get a text number (it's only 1$ a month)

Change those settings in your __init__.py file to what you need

## Install

First, make your tasks in data/tasks.py based on data/tasks_example.py

Run ./install.sh on your server, which will not only install it but start the jobs

On the server, schedule the restart job each minute or so to pull new code and restart the scheduler:

    crontab -e
    * * * * * /bin/bash /src/virtual_assistant/restart.sh

## TODO

Next, it's time to learn Vue.js by building a slick UI for this puppy

## Links


<https://schedule.readthedocs.io/en/stable/faq.html>
<https://github.com/dbader/schedule>
