import unittest

# from email_client import EmailClient

""" the email client I normally used was outdated, but this is how I tested it before.
Recreate something like this to test the email client grabbed from stackoverflow """


# class EmailClientTests(unittest.TestCase):
#
#     def setUp(self):
#         self.client = EmailClient()
#
#     def test_send_mail(self):
#         self.client.configure_and_login_smtp_obj()
#         response = self.client.send_mail(THD_EMAIL, 'Subject: test email\ntest email')
#
#     def test_configure_and_login_smtp_obj(self):
#         self.client.configure_and_login_smtp_obj()
#         assert "at your service" in self.client.smtp_obj.ehlo_resp
#
#     def test_login_smtp_obj(self):
#         self.client.create_smtp_object()
#         self.client.ello()
#         response = self.client.login_smtp_obj()
#         assert response[0] == 235
#
#     def test_ello(self):
#         self.client.create_smtp_object()
#         response = self.client.ello()
#         assert response[0] == 250
#
#     def test_create_smtp_object(self):
#         response = self.client.create_smtp_object()
#         assert response[0] == 220
